

$(document).on("click", ".tabs-title", function (event) {
  if (!$(event.target).hasClass("active")) {
    if ($(".tabs-title").hasClass("active")) {
      $(".tabs-title").removeClass("active");
      $(".tabs-content li").slideUp(500);
    }

    $(event.target).toggleClass("active");

    setTimeout(
      () => $(`[data-tab-content = ${$(event.target).text()}]`).prop('hidden', false).slideDown(500),
      500
    );
  }
});
