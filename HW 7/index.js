// 1. Метод forEarch перебирає весь масив, в якому аргументами може бути індекс, елемент масиву і сам масив. 
// 2. array.length = 0;
// 3. Array.isArray(array)  / true


const someArray = ['hello', 'world', 23, '23', null]

const filterBy = (array, type) => {
    const filterArray = array.filter(elem => typeof elem !== type);
    return filterArray;
};

console.log(filterBy(someArray, 'number'));

