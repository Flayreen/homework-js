
const allImages = document.getElementsByClassName('image-to-show');

let counter = 1;
let showImages;
function showPictures() {
    showImages = setInterval(() => {
        for (let image of allImages) {
            if (!image.hasAttribute('hidden')) {
                image.setAttribute('hidden', '')
            }
        }

        if (counter === allImages.length) {
            counter = 0;
        }

        const image = allImages[counter++];
        image.removeAttribute('hidden');
    }, 1000);
}

showPictures();




const stopButton = document.querySelector('.stop-button');
const continueButton = document.querySelector('.continue-button');

function clickStopButton() {    
    clearInterval(showImages);
    stopButton.setAttribute('disabled', '');
    stopButton.classList.add('disabled');
    continueButton.classList.remove('disabled');
    continueButton.removeAttribute('disabled');
};


function clickContinueButton() {
    showPictures();
    continueButton.setAttribute('disabled', '');
    stopButton.classList.remove('disabled');
    stopButton.removeAttribute('disabled');
    continueButton.classList.add('disabled');
};



stopButton.addEventListener('click', clickStopButton);
continueButton.addEventListener('click', clickContinueButton);


