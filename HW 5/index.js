// 1. Метод обʼєкту - це функція всередині обʼєкта.
// 2. Будь-який тип даних може бути значенням обєкту
// 3. Обʼєкт - це посилальний тип даних, томущо сама перемінна цього обєкту виступає силкою, яка веде до обєкта. Тобто, якщо ми один обєкт присвоїмо через = присвоїмо іншій консанті, то це буде один і той же обєкт, просто з різними силками.



const createNewUser = () => {
    let name;
    let lastName;

    do {
        name = prompt('Enter your name');
    } while (!isNaN(name) || name === null || name === '');

    do {
        lastName = prompt('Enter your last name');
    } while (!isNaN(lastName) || lastName === null || lastName === '');

    const newUser = {
        name,
        lastName,
        getLogin() {
            let login = this.name[0].toLowerCase() + this.lastName.toLowerCase();
            return login;
        },

        set userName(newName) {
            return this.userName = newName;
        },
        set userLastName(newLastName) {
            return this.lastName = newLastName;
        },
    }

    return newUser;
};


console.log(createNewUser());




