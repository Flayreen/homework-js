const table = document.querySelector('.table');
const allCells = document.getElementsByClassName('cell_row-table');
const button = document.querySelector('button');

function createMines(mines) {
    let n = 0;
    for (let i = 1; i <= 100; i++) {
        const randomCell = Math.floor(Math.random() * allCells.length);
        if (!mines[randomCell].hasChildNodes()) {
            const mine = document.createElement('img');
            mine.classList.add('mine-img')
            mine.setAttribute('src', './images/mine.png');
            mine.setAttribute('width', '40px');
            mine.setAttribute('hidden', '');
            mines[randomCell].prepend(mine);
            ++n;
        };

        if (n === 10) {
            break;
        };
    };
};

function createNumber(cells) {
    const array = Array.from(cells)
    array.forEach(elem => {
        if (elem.innerHTML === '') {
            let countMines = 0;
            const indexOfCell = array.indexOf(elem);

            const cellNearbyCenter = [
                indexOfCell - 9,
                indexOfCell - 8,
                indexOfCell - 7,
                indexOfCell - 1,
                indexOfCell + 1,
                indexOfCell + 7,
                indexOfCell + 8,
                indexOfCell + 9,
            ];

            const cellNearbyLeft = [
                indexOfCell - 8,
                indexOfCell - 7,
                indexOfCell + 1,
                indexOfCell + 8,
                indexOfCell + 9,
            ];

            const cellNearbyRight = [
                indexOfCell - 9,
                indexOfCell - 8,
                indexOfCell - 1,
                indexOfCell + 7,
                indexOfCell + 8,
            ];

            if (indexOfCell === 0 || indexOfCell === 8 || indexOfCell === 16 || indexOfCell === 24 || indexOfCell === 32 || indexOfCell === 40 || indexOfCell === 48 || indexOfCell === 56) {
                for (let index of cellNearbyLeft) {
                    const isMine = array[index];
                    if (isMine && isMine.querySelector('.mine-img')) {
                        countMines++;
                    }
                }
            } else if (indexOfCell === 7 || indexOfCell === 15 || indexOfCell === 23 || indexOfCell === 31 || indexOfCell === 49 || indexOfCell === 47 || indexOfCell === 55 || indexOfCell === 63) {
                for (let index of cellNearbyRight) {
                    const isMine = array[index];
                    if (isMine && isMine.querySelector('.mine-img')) {
                        countMines++;
                    }
                }
            } else {
                for (let index of cellNearbyCenter) {
                    const isMine = array[index];
                    if (isMine && isMine.querySelector('.mine-img')) {
                        countMines++;
                    }
                }
            }

            if (countMines === 0) {
                const div = document.createElement('div');
                div.setAttribute('hidden', '');
                div.style.cssText = `
                    width: 65px;
                    height: 65px;
                    background-color: #F3F3F3;
                `;
                elem.prepend(div);
            } else {
                const span = document.createElement('span');
                span.innerText = `${countMines}`;
                span.setAttribute('hidden', '');
                elem.prepend(span);
            }
        }
    });
}

function openNerbyCells(div, collection) {
    const allSpansArray = Array.from(collection);
    const newSpan = div.parentNode;
        
    const indexOfCell = allSpansArray.indexOf(newSpan);

    const cellNearbyCenter = [
        indexOfCell - 9,
        indexOfCell - 8,
        indexOfCell - 7,
        indexOfCell - 1,
        indexOfCell + 1,
        indexOfCell + 7,
        indexOfCell + 8,
        indexOfCell + 9,
    ];

    const cellNearbyLeft = [
        indexOfCell - 8,
        indexOfCell - 7,
        indexOfCell + 1,
        indexOfCell + 8,
        indexOfCell + 9,
    ];

    const cellNearbyRight = [
        indexOfCell - 9,
        indexOfCell - 8,
        indexOfCell - 1,
        indexOfCell + 7,
        indexOfCell + 8,
    ];


    if (indexOfCell === 0 || indexOfCell === 8 || indexOfCell === 16 || indexOfCell === 24 || indexOfCell === 32 || indexOfCell === 40 || indexOfCell === 48 || indexOfCell === 56) {
        for (let index of cellNearbyLeft) {
            const isMine = allSpansArray[index];

            if (isMine && isMine.querySelector('.cell_row-table span')) {
                isMine.querySelector('.cell_row-table span').removeAttribute('hidden');
            }
        }
    } else if (indexOfCell === 7 || indexOfCell === 15 || indexOfCell === 23 || indexOfCell === 31 || indexOfCell === 49 || indexOfCell === 47 || indexOfCell === 55 || indexOfCell === 63) {
        for (let index of cellNearbyRight) {
            const isMine = allSpansArray[index];

            if (isMine && isMine.querySelector('.cell_row-table span')) {
                isMine.querySelector('.cell_row-table span').removeAttribute('hidden');
            }
        }
    } else {
        for (let index of cellNearbyCenter) {
            const isMine = allSpansArray[index];

            if (isMine && isMine.querySelector('.cell_row-table span')) {
                isMine.querySelector('.cell_row-table span').removeAttribute('hidden');
            }

        }
    }
}

function openCell(event) {
    const allMines = document.querySelectorAll('.mine-img');
    const allNumber = document.querySelectorAll('.cell_row-table span');
    const allEmpties = document.querySelectorAll('.cell_row-table div');

    const target = event.target.closest('.cell_row-table');

    if (target.querySelector('.mine-img')) {
        allMines.forEach(elem => {
            elem.removeAttribute('hidden');
        });

        allNumber.forEach(elem => {
            elem.removeAttribute('hidden');
        });

        allEmpties.forEach(elem => {
            elem.removeAttribute('hidden');
        });

    } else {
        if (target.querySelector('.cell_row-table div')) {
            openNerbyCells(target.querySelector('.cell_row-table div'), allCells);
            target.querySelector('.cell_row-table div').removeAttribute('hidden');
        } else {
            target.querySelector('.cell_row-table span').removeAttribute('hidden');
        }
    }

    if (button.hasAttribute('hidden')) {
        button.removeAttribute('hidden');
    }
};




createMines(allCells);
createNumber(allCells);



table.addEventListener('click', openCell);


const minesCounter = document.querySelector('.mines-counter span');
let nFlags = 0;

table.addEventListener('contextmenu', function (event) {
    const cell = event.target.closest('.cell_row-table');

    if (cell.querySelector('.flag')) {
        cell.querySelector('.flag').outerHTML = '';
        minesCounter.innerText = --nFlags;
    } else if (cell.firstElementChild.hasAttribute('hidden')) {
        const flag = document.createElement('img');
        flag.classList.add('flag');
        flag.setAttribute('src', './images/flag.png');
        flag.setAttribute('width', '40px');
        cell.prepend(flag);
        
        minesCounter.innerText = ++nFlags;
    }

    event.preventDefault();
});




button.addEventListener('click', function () {
    location.reload();
})














// const test = document.querySelector('.test');

// if (test.querySelector('.inner-test')) {
//     console.log(true);
// } else {
//     console.log(false);
// }