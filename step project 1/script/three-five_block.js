// Обробник подій для табок в 3 блоці (Our Services).

$(document).on("click", ".tab_third-block", function (event) {
    if (!$(event.target).hasClass("active-tab")) {
        if ($(".tab_third-block").hasClass("active-tab")) {
            $(".tab_third-block").removeClass("active-tab");
            $(".text-photo-third-block p").slideUp(500);
        }

        $(event.target).toggleClass("active-tab");

        setTimeout(
            () => $(`[data-content-tab="${$(event.target).text()}"]`).prop('hidden', false).slideDown(500),
            500
        );

        const image = document.querySelector('.image_third-block');
        const allParagraphs = document.querySelectorAll('.text-photo-third-block p');
        allParagraphs.forEach((elem, index) => {
            if (elem.dataset.contentTab === $(event.target).text()) {
                image.setAttribute('src', `./images/wordpress/wordpress${index + 1}.jpg`)
            }
        })
    }
});


// Колекції посилань на фото для 5 блоку (Our Amazing Work)

const allImages = {
    graphicDesignCategoryImages: [
        './images/graphic-design/graphic-design1.jpg',
        './images/graphic-design/graphic-design2.jpg',
        './images/graphic-design/graphic-design3.jpg',
        './images/graphic-design/graphic-design4.jpg',
        './images/graphic-design/graphic-design5.jpg',
        './images/graphic-design/graphic-design6.jpg',
        './images/graphic-design/graphic-design7.jpg',
        './images/graphic-design/graphic-design8.jpg',
        './images/graphic-design/graphic-design9.jpg',
        './images/graphic-design/graphic-design10.jpg',
        './images/graphic-design/graphic-design11.jpg',
        './images/graphic-design/graphic-design12.jpg', 
    ],
    webDesignCategoryImages: [
        './images/web-design/web-design1.jpg',
        './images/web-design/web-design2.jpg',
        './images/web-design/web-design3.jpg',
        './images/web-design/web-design4.jpg',
        './images/web-design/web-design5.jpg',
        './images/web-design/web-design6.jpg',
        './images/web-design/web-design7.jpg',
    ],
    landingsCategoryImages: [
        './images/landing-page/landing-page1.jpg',
        './images/landing-page/landing-page2.jpg',
        './images/landing-page/landing-page3.jpg',
        './images/landing-page/landing-page4.jpg',
        './images/landing-page/landing-page5.jpg',
        './images/landing-page/landing-page6.jpg',
        './images/landing-page/landing-page7.jpg',
    ],
    wordpressCategoryImages: [
        './images/wordpress/wordpress1.jpg',
        './images/wordpress/wordpress2.jpg',
        './images/wordpress/wordpress3.jpg',
        './images/wordpress/wordpress4.jpg',
        './images/wordpress/wordpress5.jpg',
        './images/wordpress/wordpress6.jpg',
        './images/wordpress/wordpress8.jpg',
        './images/wordpress/wordpress9.jpg',
        './images/wordpress/wordpress10.jpg',
    ],
}


// Функція для створення контейнерів з фотками для 5 блоку (Our Amazing Work)

const imageBlock = document.querySelector('.image-fifth-block');
const blockForImageBlock = document.querySelector('.content_fifth-block');
let counterClick = 0;

function createImageContent(collection) {
    blockForImageBlock.innerHTML = '';

    if (typeof collection === 'object' && !Array.isArray(collection)) {
        for (let key in collection) {
            collection[key].forEach((link, index) => {
                if (index < 3) {
                    const copyBlock = imageBlock.cloneNode(true);
                    const image = copyBlock.querySelector('img');
                    image.setAttribute('src', link);
                    blockForImageBlock.append(copyBlock)
                }
            })
        }
    }

    if (Array.isArray(collection)) {
        collection.forEach((link, index) => {
            if (index < 12) {
                const copyBlock = imageBlock.cloneNode(true);
                const image = copyBlock.querySelector('img');
                image.setAttribute('src', link);
                blockForImageBlock.append(copyBlock)
            }
        });

        if (collection.length <= 12) {
            document.querySelector('.button_fifth-block').outerHTML = '';
        }
    }
};

createImageContent(allImages);

function addNextPhoto(collection) {
    if (typeof collection === 'object' && !Array.isArray(collection)) {
        for (let key in collection) {
            collection[key].forEach((link, index) => {
                if (index >= 3 && index < 6 && counterClick === 1) {
                    const copyBlock = imageBlock.cloneNode(true);
                    const image = copyBlock.querySelector('img');
                    image.setAttribute('src', link);
                    blockForImageBlock.append(copyBlock)
                }

                if ((index >= 6 && index < 9) && counterClick === 2) {
                    const copyBlock = imageBlock.cloneNode(true);
                    const image = copyBlock.querySelector('img');
                    image.setAttribute('src', link);
                    blockForImageBlock.append(copyBlock)
                }
            })
        }
    }

    if (Array.isArray(collection)) {
        collection.forEach((link, index) => {
            if (index >= 11) {
                const copyBlock = imageBlock.cloneNode(true);
                const image = copyBlock.querySelector('img');
                image.setAttribute('src', link);
                blockForImageBlock.append(copyBlock);
            }
        });
        document.querySelector('.button_fifth-block').outerHTML = '';
    }

    if (counterClick === 2) {
        document.querySelector('.button_fifth-block').outerHTML = '';
    }
}


const tabsFifthBlock = document.querySelector('.main_fifth-block');
const buttonFifthBlock = document.querySelector('.button_fifth-block');


// Обробник подій для кнопки 'load' 5 блоку (Our Amazing Work)

buttonFifthBlock.addEventListener('click', function (event) {
    counterClick ++;

    const loading = document.querySelector('.loading');
    loading.style.display = 'flex';
    document.body.style.overflow = 'hidden';
    setTimeout(() => {
        loading.style.display = 'none';
        document.body.style.overflow = '';

        const activeTab = document.querySelector('.active-fifth-tab');

        switch (activeTab.innerText.toLowerCase()) {
            case 'all':
                addNextPhoto(allImages);
                break;
            case 'graphic design':
                addNextPhoto(allImages.graphicDesignCategoryImages);
                break;
            case 'web design':
                addNextPhoto(allImages.webDesignCategoryImages);
                break;
            case 'landing pages':
                addNextPhoto(allImages.landingsCategoryImages);
                break;
            case 'wordpress':
                addNextPhoto(allImages.wordpressCategoryImages);
                break;
        }

    }, 2000);

     event.stopPropagation()
})


// Обробник подій для табок 5 блоку (Our Amazing Work)

tabsFifthBlock.addEventListener('click', function (event) {
    counterClick = 0;
    if (!document.querySelector('.button_fifth-block')) {
        document.querySelector('.main_fifth-block').append(buttonFifthBlock)
    }

    const targetTab = event.target.closest('.tab_fifth-block');

    if (targetTab && !targetTab.classList.contains('.active-fifth-tab')) {
        const activeTab = document.querySelector('.active-fifth-tab');
        activeTab.classList.remove('active-fifth-tab');

        event.target.classList.add('active-fifth-tab');

        switch (targetTab.innerText.toLowerCase()) {
            case 'all':
                createImageContent(allImages);
                break;
            case 'graphic design':
                createImageContent(allImages.graphicDesignCategoryImages);
                break;
            case 'web design':
                createImageContent(allImages.webDesignCategoryImages);
                break;
            case 'landing pages':
                createImageContent(allImages.landingsCategoryImages);
                break;
            case 'wordpress':
                createImageContent(allImages.wordpressCategoryImages);
                break;
        }
    }
})




