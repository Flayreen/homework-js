const randomImage = [
    'https://images.unsplash.com/photo-1463453091185-61582044d556?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8cmFuZG9tJTIwcGVyc29ufGVufDB8fDB8fHww&w=1000&q=80',
    'https://images.unsplash.com/photo-1481349518771-20055b2a7b24?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8cmFuZG9tfGVufDB8fDB8fHww&w=1000&q=80',
    'https://lh3.googleusercontent.com/hwau7OVWx96XaME5KpRuJ0I_MscrerK6SbRH1UwYHYaxIDQQtn7RZK02LDSfBzCreidFgDsJeXyqDct6EZiH6vsV=w640-h400-e365-rj-sc0x00ffffff',
    'https://www.thedesignwork.com/wp-content/uploads/2011/10/Random-Pictures-of-Conceptual-and-Creative-Ideas-02.jpg',
    'https://randomwordgenerator.com/img/picture-generator/53e1d2474c53a814f1dc8460962e33791c3ad6e04e50744074267bd29e45c4_640.jpg',
    'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8cmFuZG9tJTIwcGVyc29ufGVufDB8fDB8fHww&w=1000&q=80',
    'https://c8.alamy.com/comp/2C46HEW/3d-selection-of-spheres-and-random-objects-on-pedestal-in-front-of-orange-wall-3d-illustration-2C46HEW.jpg',
    'https://www.doubledtrailers.com/assets/images/random%20horse%20facts%20shareable.png',
    'https://www.thesaurus.com/e/wp-content/uploads/2022/10/20221011_randomWords_1000x700.jpg',
    'https://miro.medium.com/v2/resize:fit:1400/1*TzaiFDmkiEkOxNU7eG43pw.jpeg',
    'https://images.summitmedia-digital.com/spotph/images/2022/11/20/twesha-1200-1668944850.jpg',
    'https://post.healthline.com/wp-content/uploads/2020/08/732x549_Are_Random_Erections_Normal-1-732x549.jpg',
    'https://static01.nyt.com/images/2022/09/22/books/00PRH/00PRH-videoSixteenByNine3000.jpg',
    'https://www.instyle.com/thmb/WjJybebXu_TaITkNyEwgcoU9Wmg=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/072221-rogue-facial-hairs-lead-2000-9303eeab35a3451590d2551c644dad81.jpg',
    'https://machinelearningmastery.com/wp-content/uploads/2017/01/A-Gentle-Introduction-to-the-Random-Walk-for-Times-Series-Forecasting-with-Python.jpg',
    'https://storage.googleapis.com/pod_public/1300/88149.jpg',
    'https://publish.purewow.net/wp-content/uploads/sites/2/2021/11/random-things-to-buy-on-amazon-aquapaw.jpg?fit=728%2C524',
    'https://assets.byub.org/images/7b10c9b8-e94f-464c-abbf-198a21f09399/1280x720.webp',
    'https://i.ytimg.com/vi/jg9Wiul_0vY/maxresdefault.jpg',
    'https://ichef.bbci.co.uk/news/976/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg',
    'https://uploads.sitepoint.com/wp-content/uploads/2022/09/1663565358random-numbers-js.jpg',
    'https://assets.randomactsofkindness.org/site/mktn.jpg',
    'https://hatrabbits.com/wp-content/uploads/2017/01/random-word-1.jpg',
    'https://files.realpython.com/media/random_data_watermark.576078a4008d.jpg',
];


// Останній блок

const allItemsLastBlock = document.querySelectorAll('.item');

allItemsLastBlock.forEach((block, index) => {
    if (index >= 1) {
        const copyHoverblock = document.querySelector('.item-hover').cloneNode(true);
        block.append(copyHoverblock);
    }
})

const buttonEighthBlock = document.querySelector('.button_eighth-block');

buttonEighthBlock.addEventListener('click', function (event) {
    const loading = document.querySelector('.loading');
    loading.style.display = 'flex';
    document.body.style.overflow = 'hidden';

    setTimeout(() => {
        loading.style.display = 'none';
        document.body.style.overflow = '';

        const copyGallery = document.querySelector('.gallery_eighth-block').cloneNode(true);
        document.querySelector('.gallery_eighth-block').append(copyGallery);
        buttonEighthBlock.outerHTML = '';

        let number = 0;
        const allImagesCopyGallery = copyGallery.querySelectorAll('img');
        allImagesCopyGallery.forEach(img => {
            ++number;
            img.setAttribute('src', randomImage[number])
        })
    }, 2000);

})