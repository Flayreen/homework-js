// Добавлення усіх карточок для 6 блоку (Breaking News)

const contentSixthBlock = document.querySelector('.content_sixth-block');

for (let i = 1; i < 8; i++) {
    const copySixthItem = document.querySelector('.item-sixth-block').cloneNode(true);
    const image = copySixthItem.querySelector('img');
    image.setAttribute('src', `./images/six-block-image/img${i}.png`);
    contentSixthBlock.append(copySixthItem);
}


function setMemberInfo(member) {
    const nextImage = member.querySelector('img');
    const selectedImage = document.querySelector('.selected-member img');
    selectedImage.setAttribute('src', `${nextImage.getAttribute('src')}`);

    const membersName = document.querySelector('.personal-info_seventh-block h4');
    membersName.innerText = member.dataset.name;

    const membersJob = document.querySelector('.personal-info_seventh-block span');
    membersJob.innerText = member.dataset.job;

    const membersDescripton = document.querySelector('.text_seventh-block p');
    membersDescripton.innerText = member.dataset.description;
}


// Пагінація працівників компанії

const membersBlock = document.querySelector('.all-members');

membersBlock.addEventListener('click', function (event) {
    const targetArrow = event.target.closest('.pagination');
    const checkedMember = document.querySelector('.checked-member');
    checkedMember.classList.remove('checked-member');

    if (targetArrow) {
        if (targetArrow.classList.contains('arrow-right')) {
            const nextMember = checkedMember.nextElementSibling;
            if (!nextMember.classList.contains('member')) {
                const activeMember = document.querySelector('.first-member');
                activeMember.classList.add('checked-member');
                setMemberInfo(activeMember);
            } else {
                nextMember.classList.add('checked-member');
                setMemberInfo(nextMember);
            }
            
        }

        if (targetArrow.classList.contains('arrow-left')) {
            const previousMember = checkedMember.previousElementSibling;
            if (!previousMember.classList.contains('member')) {
                const activeMember = document.querySelector('.last-member');
                activeMember.classList.add('checked-member');
                setMemberInfo(activeMember);
            } else {
                previousMember.classList.add('checked-member');
                setMemberInfo(nextMember);
            }
        }
    };

    const targetMember = event.target.closest('.member');

    if (targetMember && !targetMember.classList.contains('checked-member')) {
        targetMember.classList.add('checked-member');
        setMemberInfo(targetMember);
    }
})
