
const body = document.body;
const strBody = body.innerHTML;


localStorage.setItem('mainTheme', strBody);
localStorage.removeItem('name');

function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}





function changeColor(parent) {
    if (parent !== document.body && parent !== document.querySelector('main')) {
        parent.style.color = getRandomColor();
        parent.style.backgroundColor = getRandomColor();
    }

    for (let elem of parent.children) {
        if (elem.children.length > 0) {
            changeColor(elem);
        }
    }
}


function removeColors(parent) {
    parent.removeAttribute('style');

    for (let elem of parent.children) {
        elem.removeAttribute('style');

        if (elem.children.length > 0) {
            for (let index of elem.children) {
                removeColors(index)
            }
        }
    }
}




const changeButton = document.querySelector('.change-theme_button');

changeButton.addEventListener('click', () => {
    if (body.innerHTML === localStorage.getItem('mainTheme')) {
        return changeColor(body);
    } 

    if (body.innerHTML !== localStorage.getItem('mainTheme')) {
        return removeColors(body);
    } 
})