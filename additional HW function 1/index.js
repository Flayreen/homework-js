// 1. Рекурсія - це викликання функції самї же себе. Вона потрібна для того, що функція виконувалася декілька разів.

let amount;
do {
    amount = +prompt('Enter the amount to calculate the factorial')
} while (isNaN(amount) || amount === '' || amount === null);


const getFactorial = number => {
    if (number === 1) {
        return 1;
    } 

    return number * getFactorial(number - 1) ;
};

console.log(getFactorial(amount));

5 * 4 * 3 * 2