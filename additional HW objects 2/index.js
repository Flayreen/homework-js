
const user = {
    name: 'Oleg',
    surname: 'Vakarchuk',
    age: 23,
    location: {
        city: 'Lviv',
        address: ['2Pac', 11]
    }
};

console.log(user);


function cloneObject(obj) {
    const newObject = {};

    for (let key in obj) {
        newObject[key] = obj[key];

        if (typeof obj[key] === 'object') {
            cloneObject(obj[key]);
        }
    }

    return newObject;
}

console.log(cloneObject(user));