// 1. Цикли потрібні для того, щоб робити якусь дію декілька разів поки не буде виконана умова.
// 2. Цикл for - код виконується для кожної ітерації. Цикло do ... while спочатку виконує код, а потім перевіряє його правильність умови. While виконує код, поки дійнсна умова.
// 3. Неявне перетворення - це автоматичне перетворення одних типів даних в інші.
// Приклад: console.log('10' / 5); result 2

let number;

do {
    number = +prompt('Enter number');
} while (isNaN(number) || number === '' || number === null || !Number.isInteger(number));


if (number >= 5) {
    for (let i = 1; i <= number; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
} else {
    console.log('Sorry, no numbers');
}










