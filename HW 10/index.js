
const tabs = document.querySelector('.tabs');


tabs.addEventListener('click', function (event) {
    if (event.target.closest('.tabs-title').classList.contains('active')) {
       return;
    } else {
        const activeLI = document.querySelector('li.active');
        activeLI.classList.remove('active');

        event.target.closest('.tabs-title').classList.add('active');

        const allText = document.querySelectorAll('.tabs-content li');
        allText.forEach(elem => {
            if (elem.dataset.tabContent === event.target.closest('.tabs-title').innerText && elem.hasAttribute('hidden')) {
                elem.removeAttribute('hidden');
            };

            if (elem.dataset.tabContent !== event.target.closest('.tabs-title').innerText && !elem.hasAttribute('hidden')) {
                elem.setAttribute('hidden', '');
            };
        })

    }
})