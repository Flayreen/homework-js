
const storePoints = [1, 5, 9, 8, 2]; /// storePoints.lenght - кількість розробників

const backlogs = [5, 2, 9, 15, 11, 4, 10, 12]; /// backlog.lenght - кількість завдань

const deadline = new Date('2023.06.28');

const calculateDeadline = (team, tasks, deadline) => {
    const pointsPerDay = team.reduce((accum, value) => accum + value, 0);
    const pointsForBacklogs = tasks.reduce((accum, value) => accum + value, 0);
    const daysForBacklogs = pointsForBacklogs / pointsPerDay;

    const today = new Date();
    const daysDifference = (deadline - today) / (1000 * 60 * 60 * 24);

    if (daysForBacklogs <= daysDifference) {
        return alert(`Усі завдання будуть успішно виконані за ${Math.round(daysDifference - daysForBacklogs)} днів до настання дедлайну!`);
    } else {
        return alert(`Команді розробників доведеться витратити додатково ${Math.round((daysForBacklogs - daysDifference) * 8)} годин після дедлайну, щоб виконати всі завдання в беклозі`);
    }

};

console.log(calculateDeadline(storePoints, backlogs, deadline));
