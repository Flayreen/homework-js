
const calculateFibonacci = (f0, f1, n) => {
    if (n === 0) {
        return f0;
    } else if (n === 1) {
        return f1;
    } else if (n > 1) {
        return calculateFibonacci(f0, f1, n - 1) + calculateFibonacci(f0, f1, n - 2);
    } else {
        return calculateFibonacci(f0, f1, n + 2) - calculateFibonacci(f0, f1, n + 2)
    }
}


console.log(calculateFibonacci(1, 3, -2));

