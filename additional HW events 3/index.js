
// Create table
const table = document.createElement('table');
document.body.prepend(table);

const tableBody = document.createElement('tbody');
table.prepend(tableBody);

for (let i = 1; i <= 30; i++) {
    const tableRow = document.createElement('tr');
    for (let j = 1; j <= 30; j++) {
        const tableCell = document.createElement('td');
        tableCell.style.backgroundColor = 'white';
        tableCell.style.width = '30px';
        tableCell.style.height = '30px';
        tableCell.style.border = '1px solid black';
        tableRow.append(tableCell);
    }
    tableBody.append(tableRow);
};


// Change color of one cell
table.addEventListener('click', function (event) {
    if (event.target.closest('td')) {
        if (event.target.style.backgroundColor === 'white') {
            event.target.style.backgroundColor = 'black';
        } else {
            event.target.style.backgroundColor = 'white';
        };
    };

    event.stopPropagation();
});


// Change color of all cells
document.body.addEventListener('click', function (event) {
    const allCels = document.querySelectorAll('td');

    allCels.forEach(cell => {
        if (cell.style.backgroundColor === 'white') {
            cell.style.backgroundColor = 'black';
        } else {
            cell.style.backgroundColor = 'white';
        };
    });
});