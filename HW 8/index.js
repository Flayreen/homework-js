// 1. DOM - це технологія, яка представляє html дерево і дає нам можливість змінювати його.
// 2. innerHTML змінює увесь html код всередині тега, innerText змінює лише текст
// 3. quertSelectorAll, quertSelector, getElementById, getElementsByTagName, getElementsByName, getElementsByClassName.




// Task 1
const allParagraphs = document.querySelectorAll('p');
allParagraphs.forEach(elem => elem.style.backgroundColor = '#ff0000');


// Task 2
const otionsList = document.getElementById('optionsList');
console.log(otionsList);

const parentOtionsList = otionsList.parentNode;
console.log(parentOtionsList);

const nodesOtionsList = otionsList.childNodes;
nodesOtionsList.forEach(elem => console.log(elem));


// Task 3
const testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = 'This is a paragraph';


// Task 4
const mainHeader = document.querySelector('.main-header');

const childensHeader = mainHeader.children;

for (let key of childensHeader) {
    key.classList.add('nav-item');
}


// Task 5
const sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle);

for (let key of sectionTitle) {
    key.classList.remove('section-title');
}

