// 1. const newElem = document.createElement('div');
// 2. Перший парамент визначає, куди відбудеться вставка елемента. Може бути beforebegin, beforeend, afterend, afterbegin
// 3. батьківський елемент.removeChild(дочірній елемент) або елемент.outerHTML = '';


const itemsList = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const blockList = document.querySelector('.block_list');


function createList(array, parent = document.body) {
    const list = document.createElement('ul');

    array.forEach(elem => {
        if (Array.isArray(elem)) {
            const innerList = document.createElement('ul');
            elem.forEach(elem => {
                const li = document.createElement('li');
                li.innerText = `${elem}`;
                innerList.append(li);
            });

            list.append(innerList);

        } else {
            const li = document.createElement('li');
            li.innerText = `${elem}`;
            list.append(li);
        }
    });
    
    parent.prepend(list);
}

createList(itemsList, blockList);



// function clearPage(sec) {
//     if (sec === 0) {
//         document.body.innerHTML = ''
//         return console.log(0);;
//     }

//     console.log(sec);
//     setTimeout(function () {
//         clearPage(sec - 1);
//     }, 1000);

// }

// clearPage(3);




