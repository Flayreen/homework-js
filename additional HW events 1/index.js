const mainButton = document.querySelector('.btn');

function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}

mainButton.addEventListener('click', function () {
    document.body.innerHTML = '';

    const labelForInput = document.createElement('label');
    labelForInput.innerText = 'Ввведіть діаметр кола   ';
    const input = document.createElement('input');
    labelForInput.append(input);
    document.body.prepend(labelForInput);
    
    const secondButton = document.createElement('button');
    secondButton.innerText = 'Намалювати 100 кіл';
    secondButton.style.marginLeft = '10px'
    document.body.append(secondButton);

    secondButton.addEventListener('click', function(event) {
        if (input.value !== '' && !isNaN(input.value) && !input.value.includes(' ')) {
            secondButton.setAttribute('disabled', '');
            input.setAttribute('disabled', '');
            
            const blockForCircles = document.createElement('div');
            blockForCircles.style.cssText = `
                display: flex;
                flex-direction: column;
                gap: 10px;
                margin-bottom: 20px;
            `
            document.body.prepend(blockForCircles);

            for (let i = 1; i <= 10; i++) {
                const circlesRow = document.createElement('div');
                circlesRow.classList.add('circle-row');
                circlesRow.style.cssText = `
                    display: flex;
                    juctify-content: flex-start;
                    gap: 10px;
                `
                for (let j = 1; j <= 10; j++) {
                    const circle = document.createElement('div');
                    circle.classList.add('inner-circle');
                    circle.style.cssText = `
                        width: ${input.value}px;
                        height: ${input.value}px;
                        border-radius: 99px;
                        background-color: ${getRandomColor()};
                    `
                    circlesRow.append(circle);
                }
                blockForCircles.append(circlesRow);
            }

            blockForCircles.addEventListener('click', function (event) {

                if (event.target.closest('.inner-circle')) {
                    event.target.outerHTML = '';
                }

                const blockCirclesRow = document.querySelectorAll('.circle-row');
                blockCirclesRow.forEach(elem => {
                    if (elem.children.length === 0) {
                        elem.outerHTML = '';
                    }
                })

                if (blockForCircles.children.length === 0) {
                    blockForCircles.outerHTML = '';
                    secondButton.removeAttribute('disabled');
                    input.removeAttribute('disabled');
                }
            })
        }
        
    })

})


// const mainButton = document.querySelector('.btn');

// function getRandomColor() {
//   const r = Math.floor(Math.random() * 255);
//   const g = Math.floor(Math.random() * 255);
//   const b = Math.floor(Math.random() * 255);

//   return `rgb(${r}, ${g}, ${b})`;
// }

// mainButton.addEventListener('click', function () {
//     document.body.innerHTML = '';

//     const labelForInput = document.createElement('label');
//     labelForInput.innerText = 'Ввведіть діаметр кола   ';
//     const input = document.createElement('input');
//     labelForInput.append(input);
//     document.body.prepend(labelForInput);
    
//     const secondButton = document.createElement('button');
//     secondButton.innerText = 'Намалювати 100 кіл';
//     secondButton.style.marginLeft = '10px'
//     document.body.append(secondButton);

//     secondButton.addEventListener('click', function(event) {
//         if (input.value !== '' && !isNaN(input.value) && !input.value.includes(' ')) {
//             secondButton.setAttribute('disabled', '');
//             input.setAttribute('disabled', '');
            
//             const blockForCircles = document.createElement('div');
//             blockForCircles.style.cssText = `
//                 display: grid;
//                 grid-template-columns: repeat(10, ${input.value}px);
//                 grid-template-rows: repeat(10, ${input.value}px);
//                 grid-gap: 5px;
//                 margin-bottom: 20px
//             `
//             document.body.prepend(blockForCircles);

//             for (let i = 1; i <= 1; i++) {
//                 const div = document.createElement('div');
//                 div.classList.add('inner-circle');
//                 div.style.cssText = `
//                     width: ${input.value}px;
//                     height: ${input.value}px;
//                     border-radius: 99px;
//                     background-color: ${getRandomColor()};
//                 `
//                 blockForCircles.append(div);
//             }

//             blockForCircles.addEventListener('click', function (event) {
//                 if (event.target.closest('.inner-circle')) {
//                     event.target.outerHTML = '';
//                 }

//                 if (blockForCircles.children.length === 0) {
//                     blockForCircles.outerHTML = '';
//                     secondButton.removeAttribute('disabled');
//                     input.removeAttribute('disabled');
//                 }
//             })
//         }
//     })
// })


