
const passwordForm = document.querySelector('.password-form');

const passwordField = document.querySelector('#password');
const confirmPasswordField = document.querySelector('#confirm-password');
const btnSubmit = document.querySelector('.btn');



passwordForm.addEventListener('click', function (event) {
    if (event.target.closest('.fas')) {
        if (event.target.classList.contains('fa-eye-slash')) {
            event.target.classList.remove('fa-eye-slash');
            event.target.classList.add('fa-eye');
            event.target.previousElementSibling.setAttribute('type', 'text');
        } else {
            event.target.classList.remove('fa-eye');
            event.target.classList.add('fa-eye-slash');
            event.target.previousElementSibling.setAttribute('type', 'password');
        }
    }
})



btnSubmit.addEventListener('click', function () {
    if (passwordField.value === confirmPasswordField.value) {
        alert('You are welcome');
    } else {
        alert('Потрібно ввести однакові значення');
    }
})

