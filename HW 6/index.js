

const createNewUser = () => {
    let name;
    let lastName;
    let birthday;

    do {
        name = prompt('Enter your name');
    } while (!isNaN(name) || name === null || name === '');

    do {
        lastName = prompt('Enter your last name');
    } while (!isNaN(lastName) || lastName === null || lastName === '');

    do {
        birthday = prompt('Enter your birthday (dd.mm.yyyy)');
    } while (birthday.length !== 10);

    const newUser = {
        name,
        lastName,
        birthday,
        getAge() {
            const today = new Date();
            const userBirthday = new Date(this.birthday.slice(-4) + this.birthday.slice(2, 6) + this.birthday.slice(0, 2));
            let age = 0;

            if (userBirthday.getFullYear() > today.getFullYear()) {
                console.log('The user has not been born yet');
            } else {
                age += today.getFullYear() - userBirthday.getFullYear();
            };

            if ((userBirthday.getMonth() === today.getMonth() && userBirthday.getDate() > today.getDate()) || (userBirthday.getMonth() > today.getMonth())) {
                age -= 1;
            };
            return age;
        },

        getPassword() {
            let password = this.name[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
            return password;
        },

        getLogin() {
            let login = this.name[0].toLowerCase() + this.lastName.toLowerCase();
            return login;
        },

        set userName(newName) {
            return this.userName = newName;
        },
        set userLastName(newLastName) {
            return this.lastName = newLastName;
        },

    };

    return [newUser.getAge(), newUser.getPassword()];
};


console.log(createNewUser());







