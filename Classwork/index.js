/**
 * Завдання 2.
 *
 * Напишіть програму "Кавова машина".
 *
 * Програма приймає монети та готує напої:
 * - кава за 25 монет;
 * - Капучіно за 50 монет;
 * - Чай за 10 монет.
 *
 * Щоб програма дізналася, що робити, вона повинна знати:
 * - Скільки монет користувач вніс;
 * - Який він бажає напій.
 *
 * Залежно від того, який напій вибрав користувач,
 * програма повинна обчислити здачу та вивести повідомлення в консоль:
 * «Ваш «НАЗВА НАПОЮ» готовий. Візьміть здачу: "СУМА ЗДАЧІ".".
 *
 *Якщо користувач ввів суму без здачі, вивести повідомлення:
 * «Ваш «НАЗВА НАПОЮ» готовий. Дякую за суму без здачі! :)"
 */


// const coffeeMachine = (monets, product) => {
//     switch (product) {
//         case 'Кава':
//             if (monets === 25) {
//                 console.log('Ваша кава готова. Дякую за суму без здачі');
//             } else {
//                 let result = monets - 25;
//                 console.log(`Ваша кава готова. Решта: ${result} монет`);
//             }
//             break;
//         case 'Капучіно':
//             if (monets === 50) {
//                 console.log('Ваш капучіно готовий. Дякую за суму без здачі');
//             } else {
//                 let result = monets - 50;
//                 console.log(`Ваш капучіно готовий. Решта: ${result} монет`);
//             }
//             break;
//         case 'Чай':
//             if (monets === 10) {
//                 console.log('Ваш чай готовий. Дякую за суму без здачі');
//             } else {
//                 let result = monets - 10;
//                 console.log(`Ваш чай готовий. Решта: ${result} монет`);
//             }
//             break;
//     }
// };

// coffeeMachine(100, 'Капучіно');



// ТЕРНАРНИЙ ОПЕРАТОР!!

// const coffeeMachine = (monets, product) => {
//     switch (product) {
//         case 'Кава':
//             monets - 25 === 0 ? console.log('Ваша кава готова. Дякую за суму без здачі') : console.log(`Ваша кава готова. Решта: ${monets - 25} монет`);
//             break;
//         case 'Капучіно':
//             monets - 50 === 0 ? console.log('Ваш Капучіно готовий. Дякую за суму без здачі') : console.log(`Ваш Капучіно готовий. Решта: ${monets - 50} монет`);
//             break;
//         case 'Чай':
//             monets - 10 === 0 ? console.log('Ваш чай готовий. Дякую за суму без здачі') : console.log(`Ваш чай готовий. Решта: ${monets - 10} монет`);
//             break;
//     };
// };

// coffeeMachine(50, 'Капучіно');






/**
 * Завдання 3.
 *
 * Написати функцію, яка обраховує факторіал.
 *
**/

// function factorial(n) {
//     if (n === 1) {
//         return 1;
//     };

//     return n * factorial(n - 1);
// };

// console.log(factorial(5));



/**
 * Завдання 2.
 *
 * У нас є об’єкт для зберігання заробітної плати нашої команди:
 *
 * Напишіть код для підсумовування всіх зарплат і збережіть у змінній sum. У наведеному вище прикладі має бути 790.
 *
 */

// let salaries = {
//   Anna: 150,
//   Oleksandr: 210,
//   Julia: 430,
// };

// let sum = 0;

// for (let key in salaries) {
//     sum += salaries[key];
// };

// console.log(sum);

/**
 * Завдання 3.
 *
 * Створіть функцію multiplyNumeric(obj), яка примножує всі числові властивості об’єкта obj на 2.
 *
 */

// let menu = {
//   width: 200,
//   height: 300,
//   title: "Моє меню",
// };

// function multiplyNumeric(obj) {
//   const newObj = { ...obj };
//   for (let key in newObj) {
//     if (typeof newObj[key] === 'number') {
//       newObj[key] = newObj[key] * 2;
//     };
//   };

//   return newObj;
// };

// console.log(multiplyNumeric(menu));




/**
 * Завдання 4.
 *
 * За допомогою циклу for...in вивести в консоль усі властивості
 * першого рівня об'єкта у форматі «ключ-значення».
 *
 * Просунута складність:
 * Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.
 */

/* Дано */

// const user = {
//   firstName: "Walter",
//   lastName: "White",
//   job: "Programmer",
//   pets: {
//     cat: "Kitty",
//     dog: "Doggy",
//   },
// };


// for (let key in user) {
//   if (typeof user[key] !== 'object') {
//     console.log(`${key} - ${user[key]}`);
//   };

//   if (typeof user[key] === 'object') {
//     for (let keys in user[key]) {
//       console.log(`${keys} - ${user[key][keys]}`);
//     }
//   };
// };


/**
 * Завдання 5.
 *
 * Написати функцію-помічник для ресторану.
 *
 * Функція має два параметри:
 * - Розмір замовлення (small, medium, large);
 * - Тип обіду (breakfast, lunch, dinner).
 *
 * Функція повертає об'єкт із двома полями:
 * - totalPrice - загальна сума страви з урахуванням її розміру та типу;
 * - totalCalories — кількість калорій, що міститься у блюді з урахуванням його розміру та типу.
 *
 * Нотатки:
 * - Додаткові перевірки робити не потрібно;
 * - У рішенні використовувати референтний об'єкт з цінами та калоріями.
 */

/* Дано */

// const priceList = {
//   sizes: {
//     small: {
//       price: 15,
//       calories: 250,
//     },
//     medium: {
//       price: 25,
//       calories: 340,
//     },
//     large: {
//       price: 35,
//       calories: 440,
//     },
//   },
//   types: {
//     breakfast: {
//       price: 4,
//       calories: 25,
//     },
//     lunch: {
//       price: 5,
//       calories: 5,
//     },
//     dinner: {
//       price: 10,
//       calories: 50,
//     },
//   },
// };

// const getOrder = (size, type) => {

//   let totalPrice = 0;
//   totalPrice += priceList.sizes[size].price += priceList.types[type].price;

//   let totalCalories = 0;
//   totalCalories += priceList.sizes[size].calories += priceList.types[type].calories;

//   const someObj = {
//     totalPrice,
//     totalCalories,
//   };

//   return someObj;
// };

// console.log(getOrder('medium', 'lunch'));



/**
 * Завдання 6.
 *
 * Написати функцію-фабрику, яка повертає об'єкти користувачів.
 *
 * Об'єкт користувача має три властивості:
 * - Ім'я;
 * - Прізвище;
 * - Професія.
 *
 * Функція-фабрика в свою чергу має три параметри,
 * які відбивають вищеописані властивості об'єкта.
 * Кожен параметр функції має значення за замовчуванням: null.
 */




