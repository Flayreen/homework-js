const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', function (event) {
    buttons.forEach(button => {
        button.style.backgroundColor = 'black';

        if (button.innerText.toLowerCase() === event.key || button.innerText === event.key) {
            button.style.backgroundColor = 'blue';
        }
    })
})